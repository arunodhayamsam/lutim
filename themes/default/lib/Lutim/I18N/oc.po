# Lutim language file
# Copyright (C) 2014 Luc Didry
# This file is distributed under the same license as the Lutim package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Luc Didry <luc@framasoft.org>, 2018. #zanata
# Quentí, 2018. #zanata
# Luc Didry <luc@framasoft.org>, 2019. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2020-03-09 16:30+0000\n"
"Last-Translator: Quentin PAGÈS <quentinantonin@free.fr>\n"
"Language-Team: Occitan <https://weblate.framasoft.org/projects/lutim/"
"default-theme/oc/>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.2\n"

#. (7)
#. (30)
#. ($delay)
#. (config('max_delay')
#: lib/Lutim/Command/cron/stats.pm:159 lib/Lutim/Command/cron/stats.pm:160 lib/Lutim/Command/cron/stats.pm:173 lib/Lutim/Command/cron/stats.pm:174 lib/Lutim/Command/cron/stats.pm:190 lib/Lutim/Command/cron/stats.pm:191 themes/default/templates/myfiles.html.ep:24 themes/default/templates/myfiles.html.ep:33 themes/default/templates/myfiles.html.ep:34 themes/default/templates/partial/for_my_delay.html.ep:13 themes/default/templates/partial/for_my_delay.html.ep:14 themes/default/templates/partial/for_my_delay.html.ep:4 themes/default/templates/partial/lutim.js.ep:140 themes/default/templates/partial/lutim.js.ep:149 themes/default/templates/partial/lutim.js.ep:150 themes/default/templates/partial/raw.js.ep:23 themes/default/templates/partial/raw.js.ep:24 themes/default/templates/partial/raw.js.ep:6 themes/default/templates/partial/raw.js.ep:7 themes/default/templates/raw.html.ep:8 themes/default/templates/raw.html.ep:9
msgid "%1 days"
msgstr "%1 jorns"

#. ($total)
#: themes/default/templates/stats.html.ep:2
msgid "%1 sent images on this instance from beginning."
msgstr "%1 imatges mandats sus aquesta instància dempuèi lo començament."

#: themes/default/templates/index.html.ep:208
msgid "-or-"
msgstr "-o-"

#: lib/Lutim.pm:350 lib/Lutim/Command/cron/stats.pm:161 lib/Lutim/Command/cron/stats.pm:175 lib/Lutim/Command/cron/stats.pm:192 themes/default/templates/index.html.ep:5 themes/default/templates/myfiles.html.ep:5 themes/default/templates/partial/raw.js.ep:25 themes/default/templates/partial/raw.js.ep:8 themes/default/templates/raw.html.ep:10
msgid "1 year"
msgstr "1 an"

#: lib/Lutim.pm:349 lib/Lutim/Command/cron/stats.pm:158 lib/Lutim/Command/cron/stats.pm:172 lib/Lutim/Command/cron/stats.pm:189 themes/default/templates/index.html.ep:4 themes/default/templates/myfiles.html.ep:33 themes/default/templates/myfiles.html.ep:4 themes/default/templates/partial/for_my_delay.html.ep:13 themes/default/templates/partial/lutim.js.ep:149 themes/default/templates/partial/raw.js.ep:22 themes/default/templates/partial/raw.js.ep:5 themes/default/templates/raw.html.ep:7
msgid "24 hours"
msgstr "24 oras"

#: themes/default/templates/partial/myfiles.js.ep:180
msgid ": Error while trying to get the counter."
msgstr ": Error al moment de recuperar lo comptador."

#: themes/default/templates/partial/navbar.html.ep:77
msgid "About"
msgstr "A prepaus"

#: lib/Lutim/Command/cron/stats.pm:154 themes/default/templates/raw.html.ep:3
msgid "Active images"
msgstr "Imatges actius"

#: lib/Lutim/Controller/Image.pm:328
msgid "An error occured while downloading the image."
msgstr "Una error es apareguda pendent lo telecargament de l'imatge."

#: themes/default/templates/zip.html.ep:2
msgid "Archives download"
msgstr "Telecargar los archius"

#: themes/default/templates/about.html.ep:44 themes/default/templates/myfiles.html.ep:135 themes/default/templates/stats.html.ep:25
msgid "Back to homepage"
msgstr "Tornar a la pagina d'acuèlh"

#: themes/default/templates/index.html.ep:211 themes/default/templates/index.html.ep:212
msgid "Click to open the file browser"
msgstr "Clicatz per utilizar lo navigator de fichièr"

#: themes/default/templates/myfiles.html.ep:51
msgid "Close"
msgstr "Tampar"

#: themes/default/templates/gallery.html.ep:29
msgid "Close (Esc)"
msgstr "Tampar (Esc)"

#: themes/default/templates/about.html.ep:30
msgid "Contributors"
msgstr "Contributors"

#: themes/default/templates/partial/common.js.ep:113 themes/default/templates/partial/common.js.ep:93
msgid "Copied to clipboard"
msgstr "Copiat al quichapapièrs"

#: themes/default/templates/partial/lutim.js.ep:215 themes/default/templates/partial/lutim.js.ep:278 themes/default/templates/partial/lutim.js.ep:364
msgid "Copy all view links to clipboard"
msgstr "Copiar totes los ligams de visualizacion dins lo quichapapièrs"

#: themes/default/templates/index.html.ep:103 themes/default/templates/index.html.ep:111 themes/default/templates/index.html.ep:18 themes/default/templates/index.html.ep:36 themes/default/templates/index.html.ep:54 themes/default/templates/index.html.ep:87 themes/default/templates/index.html.ep:95 themes/default/templates/myfiles.html.ep:104 themes/default/templates/myfiles.html.ep:68 themes/default/templates/myfiles.html.ep:86 themes/default/templates/partial/common.js.ep:186 themes/default/templates/partial/lutim.js.ep:106 themes/default/templates/partial/lutim.js.ep:121 themes/default/templates/partial/lutim.js.ep:80 themes/default/templates/partial/lutim.js.ep:92 themes/default/templates/partial/myfiles.js.ep:142
msgid "Copy to clipboard"
msgstr "Copiar dins lo quichapapièrs"

#: themes/default/templates/myfiles.html.ep:123
msgid "Counter"
msgstr "Comptador"

#: themes/default/templates/stats.html.ep:18
msgid "Delay repartition chart for disabled images"
msgstr "Grafic de despartiment dels delais pels imatges desactivats"

#: themes/default/templates/stats.html.ep:15
msgid "Delay repartition chart for enabled images"
msgstr "Grafic de despartiment dels delais pels imatges activats"

#: themes/default/templates/index.html.ep:133 themes/default/templates/index.html.ep:165 themes/default/templates/index.html.ep:196 themes/default/templates/myfiles.html.ep:124 themes/default/templates/myfiles.html.ep:45 themes/default/templates/partial/lutim.js.ep:161
msgid "Delete at first view?"
msgstr "Suprimir al primièr accès ?"

#: lib/Lutim/Command/cron/stats.pm:155 themes/default/templates/raw.html.ep:4
msgid "Deleted images"
msgstr "Imatges suprimits"

#: lib/Lutim/Command/cron/stats.pm:156 themes/default/templates/raw.html.ep:5
msgid "Deleted images in 30 days"
msgstr "Imatges per èsser suprimits dins 30 jorns"

#: themes/default/templates/index.html.ep:116 themes/default/templates/myfiles.html.ep:127 themes/default/templates/partial/common.js.ep:178 themes/default/templates/partial/common.js.ep:181
msgid "Deletion link"
msgstr "Ligam de supression"

#: themes/default/templates/gallery.html.ep:10
msgid "Download all images"
msgstr "Telecargar totes los imatges"

#: themes/default/templates/index.html.ep:101 themes/default/templates/index.html.ep:99 themes/default/templates/partial/lutim.js.ep:102 themes/default/templates/partial/lutim.js.ep:98
msgid "Download link"
msgstr "Ligam de telecargament"

#: themes/default/templates/index.html.ep:28 themes/default/templates/index.html.ep:31 themes/default/templates/myfiles.html.ep:78 themes/default/templates/myfiles.html.ep:81
msgid "Download zip link"
msgstr "Ligam de telecargament de l'archiu dels imatges"

#: themes/default/templates/index.html.ep:207
msgid "Drag & drop images here"
msgstr "Lisatz e depausatz vòstres imatges aquí"

#: themes/default/templates/about.html.ep:7
msgid ""
"Drag and drop an image in the appropriate area or use the traditional way to "
"send files and Lutim will provide you four URLs. One to view the image, an "
"other to directly download it, one you can use on social networks and a last "
"to delete the image when you want."
msgstr ""
"Depausatz vòstres imatges dins la zòna prevista per aquò o seleccionatz un "
"fichièr d'un biais classic e Lutim vos donarà quatre URLs en torna. Una per "
"afichar l'imatge, una mai per lo telecargar dirèctament, una per l'utilizar "
"suls malhums socials e una darrièra per suprimir vòstre imatge quand "
"volguèssetz."

#: themes/default/templates/index.html.ep:168 themes/default/templates/index.html.ep:199
msgid "Encrypt the image (Lutim does not keep the key)."
msgstr "Chifrar l'imatge (Lutim garda pas la clau)."

#: themes/default/templates/partial/lutim.js.ep:45 themes/default/templates/partial/myfiles.js.ep:113
msgid "Error while trying to modify the image."
msgstr "Una error es apareguda al moment de modificar l'imatge."

#: themes/default/templates/stats.html.ep:10
msgid "Evolution of total files"
msgstr "Evolucion del nombre total de fichièrs"

#: themes/default/templates/myfiles.html.ep:126
msgid "Expires at"
msgstr "S'acaba lo"

#: themes/default/templates/myfiles.html.ep:112
msgid "Export localStorage data"
msgstr "Exportar las donadas localStorage"

#: themes/default/templates/myfiles.html.ep:121
msgid "File name"
msgstr "Nom del fichièr"

#: themes/default/templates/about.html.ep:24
msgid ""
"For more details, see the <a href=\"https://framagit.org/luc/"
"lutim\">homepage of the project</a>."
msgstr ""
"Per mai de detalhs, consultatz la <a href=\"https://framagit.org/luc/"
"lutim\">pagina</a> del projècte."

#: themes/default/templates/partial/navbar.html.ep:80
msgid "Fork me!"
msgstr "Tustatz-me !"

#: themes/default/templates/index.html.ep:10 themes/default/templates/index.html.ep:13 themes/default/templates/myfiles.html.ep:60 themes/default/templates/myfiles.html.ep:63
msgid "Gallery link"
msgstr "Ligam cap a la galariá"

#: themes/default/templates/partial/common.js.ep:116 themes/default/templates/partial/common.js.ep:134
msgid "Hit Ctrl+C, then Enter to copy the short link"
msgstr "Fasètz Ctrl+C puèi picatz Entrada per copiar lo ligam"

#: themes/default/templates/layouts/default.html.ep:47
msgid "Homepage"
msgstr "Acuèlh"

#: themes/default/templates/about.html.ep:20
msgid "How do you pronounce Lutim?"
msgstr "Cossí cal prononciar Lutim ?"

#: themes/default/templates/about.html.ep:6
msgid "How does it work?"
msgstr "Cossí aquò fonciona ?"

#: themes/default/templates/about.html.ep:18
msgid "How to report an image?"
msgstr "Qué far per senhalar un imatge ?"

#: themes/default/templates/about.html.ep:14
msgid ""
"If the files are deleted if you ask it while posting it, their SHA512 "
"footprint are retained."
msgstr ""
"Se los fichièrs son ben estats suprimits se o avètz demandat, lors "
"signaturas SHA512 son gardadas."

#: themes/default/templates/index.html.ep:181 themes/default/templates/index.html.ep:221
msgid "Image URL"
msgstr "URL de l'imatge"

#: lib/Lutim/Command/cron/stats.pm:153 themes/default/templates/raw.html.ep:2
msgid "Image delay"
msgstr "Delai de l'imatge"

#: themes/default/templates/partial/common.js.ep:157
msgid "Image deleted"
msgstr "Imatge suprimit"

#: lib/Lutim/Controller/Image.pm:756
msgid "Image not found."
msgstr "Imatge pas trobat."

#: themes/default/templates/myfiles.html.ep:113
msgid "Import localStorage data"
msgstr "Importar las donadas localStorage"

#: themes/default/templates/partial/navbar.html.ep:69
msgid "Informations"
msgstr "Informacions"

#: themes/default/templates/partial/navbar.html.ep:25
msgid "Install webapp"
msgstr "Installar la webapp"

#: themes/default/templates/partial/navbar.html.ep:21
msgid "Instance's statistics"
msgstr "Estatisticas de l'instància"

#: themes/default/templates/about.html.ep:11
msgid "Is it really anonymous?"
msgstr "Es vertadièrament anonim ?"

#: themes/default/templates/about.html.ep:9
msgid "Is it really free (as in free beer)?"
msgstr "Es vertadièrament gratuit ?"

#: themes/default/templates/about.html.ep:21
msgid ""
"Juste like you pronounce the French word <a href=\"https://fr.wikipedia.org/"
"wiki/Lutin\">lutin</a> (/ly.tɛ̃/)."
msgstr ""
"Òm pronóncia coma en occitan lengadocian, LU-TI-N, amb una M finala que sona "
"N, o coma la paraula francesa <a href=\"https://fr.wikipedia.org/wiki/"
"Lutin\">lutin</a> (/ly.tɛ̃/)."

#: themes/default/templates/index.html.ep:171 themes/default/templates/index.html.ep:202
msgid "Keep EXIF tags"
msgstr "Conservar las donadas EXIF"

#: themes/default/templates/partial/navbar.html.ep:43
msgid "Language"
msgstr "Lenga"

#: themes/default/templates/index.html.ep:136 themes/default/templates/index.html.ep:184 themes/default/templates/index.html.ep:224 themes/default/templates/partial/lutim.js.ep:165
msgid "Let's go!"
msgstr "Zo !"

#: themes/default/templates/partial/navbar.html.ep:74
msgid "License:"
msgstr "Licéncia :"

#: themes/default/templates/index.html.ep:107 themes/default/templates/index.html.ep:109 themes/default/templates/partial/lutim.js.ep:112 themes/default/templates/partial/lutim.js.ep:116
msgid "Link for share on social networks"
msgstr "Ligam per partejar suls malhums socials"

#: themes/default/templates/login.html.ep:8
msgid "Login"
msgstr "Connexion"

#: themes/default/templates/partial/navbar.html.ep:33
msgid "Logout"
msgstr "Desconnexion"

#: themes/default/templates/zip.html.ep:7
msgid ""
"Lutim can't zip so many images at once, so it splitted your demand in "
"multiple URLs."
msgstr ""
"Lutim pòt pas comprimir tan d'imatge d'un còp, a doncas trocejat vòstra "
"demanda en multiplas URLs."

#: themes/default/templates/about.html.ep:4
msgid ""
"Lutim is a free (as in free beer) and anonymous image hosting service. It's "
"also the name of the free (as in free speech) software which provides this "
"service."
msgstr ""
"Lutim es un servici gratuit e anonim d’albergament d’imatges. S’agís tanben "
"del nom del logicial (liure) que fornís aqueste servici."

#: themes/default/templates/about.html.ep:25
msgid "Main developers"
msgstr "Desvolopaires de l'aplicacion"

#: themes/default/templates/index.html.ep:91 themes/default/templates/index.html.ep:93 themes/default/templates/partial/lutim.js.ep:86 themes/default/templates/partial/lutim.js.ep:89
msgid "Markdown syntax"
msgstr "Sintaxi Markdown"

#: themes/default/templates/partial/myfiles.js.ep:149
msgid "Modify expiration delay"
msgstr "Modificar lo delai d'expiracion"

#: themes/default/templates/myfiles.html.ep:7 themes/default/templates/partial/navbar.html.ep:18
msgid "My images"
msgstr "Mos imatges"

#: themes/default/templates/gallery.html.ep:45
msgid "Next (arrow right)"
msgstr "Seguent (sageta dreita)"

#: themes/default/templates/partial/myfiles.js.ep:105 themes/default/templates/partial/myfiles.js.ep:132
msgid "No limit"
msgstr "Pas cap de data d'expiracion"

#: themes/default/templates/index.html.ep:183 themes/default/templates/index.html.ep:216
msgid "Only images are allowed"
msgstr "Solament son acceptats los imatges"

#: themes/default/templates/myfiles.html.ep:11
msgid ""
"Only the images sent with this browser will be listed here. The details are "
"stored in localStorage: if you delete your localStorage data, you'll loose "
"these details."
msgstr ""
"Solament los imatges mandats amb aqueste navigador seràn listats aquí. Las "
"menudalhas son gardadas en localStorage : se escafatz vòstras donadas "
"localStorage, perdretz aquelas menudalhas."

#: themes/default/templates/about.html.ep:16
msgid ""
"Only the uploader! (well, only if he's the only owner of the images' rights "
"before the upload)"
msgstr ""
"Solament lo qu'a mandat ! (ben, solament se ten los dreits exclusius dels "
"imatges abans de los mandar)"

#: themes/default/templates/login.html.ep:12
msgid "Password"
msgstr "Senhal"

#: themes/default/templates/zip.html.ep:12
msgid "Please click on each URL to download the different zip files."
msgstr ""
"Mercés de clicar sus cada URL per telecargar los diferents archius ZIP."

#. (config('contact')
#: themes/default/templates/about.html.ep:19
msgid "Please contact the administrator: %1"
msgstr "Mercés de contactar l'administrator : %1"

#: lib/Lutim/Controller/Authent.pm:36
msgid "Please, check your credentials: unable to authenticate."
msgstr ""
"Mercés de verificar vòstres identificants : impossible de vos autentificar."

#: themes/default/templates/gallery.html.ep:43
msgid "Previous (arrow left)"
msgstr "Precedent (sageta esquèrra)"

#: themes/default/templates/index.html.ep:46 themes/default/templates/index.html.ep:49 themes/default/templates/myfiles.html.ep:96 themes/default/templates/myfiles.html.ep:99
msgid "Random image link"
msgstr "Ligam cap a un imatge aleatòri"

#: themes/default/templates/stats.html.ep:22
msgid "Raw stats"
msgstr "Estatisticas brutas"

#: themes/default/templates/myfiles.html.ep:52
msgid "Save changes"
msgstr "Enregistrar las causidas"

#: themes/default/templates/index.html.ep:176
msgid "Send an image"
msgstr "Mandar un imatge"

#: themes/default/templates/login.html.ep:16 themes/default/templates/logout.html.ep:5 themes/default/templates/partial/navbar.html.ep:37
msgid "Signin"
msgstr "Connexion"

#: themes/default/templates/index.html.ep:151 themes/default/templates/partial/gallery.js.ep:211 themes/default/templates/partial/lutim.js.ep:176
msgid "Something bad happened"
msgstr "Un problèma es aparegut"

#. ($c->config('contact')
#: lib/Lutim/Controller/Image.pm:763
msgid ""
"Something went wrong when creating the zip file. Try again later or contact "
"the administrator (%1)."
msgstr ""
"Quicòm a trucat pendent la creacion de l'archiu. Mercés de tornar ensajar "
"pus tard o de contactar l'administrator (%1)."

#: themes/default/templates/partial/navbar.html.ep:55
msgid "Support the author"
msgstr "Sostenir l’autor"

#: themes/default/templates/partial/navbar.html.ep:63
msgid "Support the author on Liberapay"
msgstr "Sostenir l'autor sus Liberapay"

#: themes/default/templates/partial/navbar.html.ep:60
msgid "Support the author on Tipeee"
msgstr "Sostenir l'autor sus Tipeee"

#: themes/default/templates/about.html.ep:13
msgid ""
"The IP address of the image's sender is retained for a delay which depends "
"of the administrator's choice (for the official instance, which is located "
"in France, it's one year)."
msgstr ""
"L’IP de la persona que mandèt l'imatge es gardada pendent un delai que "
"depend de l'administrator de l'instància (per l'instància oficiala que lo "
"servidor es en França, es un delai d'un an)."

#: themes/default/templates/about.html.ep:23
msgid ""
"The Lutim software is a <a href=\"http://en.wikipedia.org/wiki/"
"Free_software\">free software</a>, which allows you to download and install "
"it on you own server. Have a look at the <a href=\"https://www.gnu.org/"
"licenses/agpl-3.0.html\">AGPL</a> to see what you can do."
msgstr ""
"Lo logicial Lutim es un <a href=\"https://oc.wikipedia.org/wiki/"
"Logicial_liure\">logicial liure</a>, que permet de lo telecargar e de l’"
"installar sus vòstre pròpri servidor. Gaitatz l’<a href=\"https://www.gnu."
"org/licenses/agpl-3.0.html\">AGPL</a> per veire que son vòstres dreits."

#: lib/Lutim/Controller/Image.pm:347
msgid "The URL is not valid."
msgstr "L'URL n'es pas valida."

#: themes/default/templates/zip.html.ep:16
msgid ""
"The automatic download process will open a tab in your browser for each link."
" You need to allow popups for Lutim."
msgstr ""
"Lo processús automatic de telecargament obrirà un onglet dins lo navigato "
"per cada ligam. Vos cal autorizar las popups per Lutim."

#: themes/default/templates/partial/myfiles.js.ep:32
msgid "The data has been successfully imported."
msgstr "Las donadas son ben estadas importadas."

#: lib/Lutim/Controller/Image.pm:160 lib/Lutim/Controller/Image.pm:228
msgid "The delete token is invalid."
msgstr "Lo geton de supression es invalid."

#. ($upload->filename)
#: lib/Lutim/Controller/Image.pm:488
msgid "The file %1 is not an image."
msgstr "Lo fichièr %1 es pas un imatge."

#. ($tx->res->max_message_size)
#. ($c->req->max_message_size)
#. (config('max_file_size')
#: lib/Lutim/Controller/Image.pm:311 lib/Lutim/Controller/Image.pm:380 themes/default/templates/partial/lutim.js.ep:249
msgid "The file exceed the size limit (%1)"
msgstr "Lo fichièr depassa lo limit de talha (%1)"

#: themes/default/templates/stats.html.ep:12
msgid "The graph's datas are not updated in real-time."
msgstr "Las donadas del grafic son pas mesas a jorn en temps real."

#. ($image->filename)
#: lib/Lutim/Controller/Image.pm:230
msgid "The image %1 has already been deleted."
msgstr "L'imatge %1 es ja estat suprimit."

#. ($image->filename)
#: lib/Lutim/Controller/Image.pm:239 lib/Lutim/Controller/Image.pm:244
msgid "The image %1 has been successfully deleted"
msgstr "L'imatge %1 es estat suprimit amb succès"

#: themes/default/templates/index.html.ep:63
msgid "The images are encrypted on the server (Lutim does not keep the key)."
msgstr "Los imatges son chifrats sul servidor (Lutim garda pas la clau)."

#: themes/default/templates/about.html.ep:5
msgid ""
"The images you post on Lutim can be stored indefinitely or be deleted at "
"first view or after a delay selected from those proposed."
msgstr ""
"Los imatges depausats sus Lutim pòdon èsser gardats sens fin, o s’escafar "
"tre lo primièr afichatge o al cap d'un delai causit entre los prepausats."

#: lib/Lutim/Controller/Image.pm:168
msgid "The image’s delay has been successfully modified"
msgstr "Lo delai de l'imatge es plan estat modificat"

#: themes/default/templates/partial/gallery.js.ep:277
msgid "There is XXXX image(s) in the gallery"
msgstr "I a XXXX imatge(s) dins la galariá"

#. ($c->config->{contact})
#: lib/Lutim/Controller/Image.pm:485
msgid "There is no more available URL. Retry or contact the administrator. %1"
msgstr ""
"I a pas mai d'URL disponiblas. Mercés de tornar ensajar o de contactar "
"l'administrator. %1"

#: themes/default/templates/gallery.html.ep:30
msgid "Toggle fullscreen"
msgstr "Passar al plen ecran"

#: themes/default/templates/partial/navbar.html.ep:8
msgid "Toggle navigation"
msgstr "Passar en navigacion"

#: lib/Lutim/Command/cron/stats.pm:162 themes/default/templates/raw.html.ep:11
msgid "Total"
msgstr "Total"

#: themes/default/templates/index.html.ep:78 themes/default/templates/partial/lutim.js.ep:17
msgid "Tweet it!"
msgstr "Tweetejatz-lo !"

#: themes/default/templates/partial/common.js.ep:110 themes/default/templates/partial/common.js.ep:90
msgid "Unable to copy to clipboard"
msgstr "Fracàs de la còpia al quichapapièrs"

#. ($short)
#: lib/Lutim/Controller/Image.pm:108 lib/Lutim/Controller/Image.pm:202 lib/Lutim/Controller/Image.pm:273
msgid "Unable to find the image %1."
msgstr "Impossible de trobar l'imatge %1."

#: lib/Lutim/Controller/Image.pm:574 lib/Lutim/Controller/Image.pm:619 lib/Lutim/Controller/Image.pm:660 lib/Lutim/Controller/Image.pm:703 lib/Lutim/Controller/Image.pm:715 lib/Lutim/Controller/Image.pm:726 lib/Lutim/Controller/Image.pm:753 lib/Lutim/Plugin/Helpers.pm:88
msgid "Unable to find the image: it has been deleted."
msgstr "Impossible de trobar l'imatge : es estat suprimit."

#: lib/Lutim/Controller/Image.pm:145
msgid "Unable to get counter"
msgstr "Impossible de recuperar lo comptador"

#: themes/default/templates/about.html.ep:17
msgid ""
"Unlike many image sharing services, you don't give us rights on uploaded "
"images."
msgstr ""
"A l'invèrse de la màger part dels servicis de partiment d'imatge, daissatz "
"pas cap de dreit suls imatges que mandatz."

#: themes/default/templates/index.html.ep:180 themes/default/templates/index.html.ep:219
msgid "Upload an image with its URL"
msgstr "Depausar un imatge per son URL"

#: themes/default/templates/myfiles.html.ep:125
msgid "Uploaded at"
msgstr "Mandat lo"

#: themes/default/templates/stats.html.ep:6
msgid "Uploaded files by days"
msgstr "Fichièrs mandats per jorn"

#. ($c->app->config('contact')
#: lib/Lutim/Plugin/Helpers.pm:221
msgid ""
"Uploading is currently disabled, please try later or contact the "
"administrator (%1)."
msgstr ""
"La mesa en linha es desactivada pel moment, mercés de tornar ensajar mai "
"tard o de contactar l'administrator (%1)."

#: themes/default/templates/index.html.ep:83 themes/default/templates/index.html.ep:85 themes/default/templates/myfiles.html.ep:122 themes/default/templates/partial/lutim.js.ep:72 themes/default/templates/partial/lutim.js.ep:76
msgid "View link"
msgstr "Ligam d'afichatge"

#: themes/default/templates/about.html.ep:22
msgid "What about the software which provides the service?"
msgstr "E a prepaus del logicial que fornís lo servici ?"

#: themes/default/templates/about.html.ep:3
msgid "What is Lutim?"
msgstr "Qu’es aquò Lutim ?"

#: themes/default/templates/about.html.ep:15
msgid "Who owns rights on images uploaded on Lutim?"
msgstr "Qual a los dreits suls imatges mandats sus Lutim ?"

#: themes/default/templates/about.html.ep:12
msgid ""
"Yes, it is! On the other side, for legal reasons, your IP address will be "
"stored when you send an image. Don't panic, it is normally the case of all "
"sites on which you send files!"
msgstr ""
"Òc, es aquò ! Al contrari, per de rasons legalas, vòstra adreça IP serà "
"enregistrada quand mandaretz un imatge. Perdetz pas lo cap, normalament es "
"çò normal pels demai dels sites suls quals mandatz de fichièrs !"

#: themes/default/templates/about.html.ep:10
msgid ""
"Yes, it is! On the other side, if you want to support the developer, you can "
"do it via <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> or via <a "
"href=\"https://liberapay.com/sky/\">Liberapay</a>."
msgstr ""
"Òc, o es ! Al contrari, s'avètz enveja de sosténer lo desvolopaire, podètz "
"far un microdon amb <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> o "
"via <a href=\"https://liberapay.com/sky/\">Liberapay</a>."

#: themes/default/templates/zip.html.ep:6
msgid "You asked to download a zip archive for too much files."
msgstr ""
"Avètz demandat de telecargar tròp d'imatges d'un còp dins un archiu ZIP."

#: themes/default/templates/about.html.ep:8
msgid ""
"You can, optionally, request that the image(s) posted on Lutim to be deleted "
"at first view (or download) or after the delay selected from those proposed."
msgstr ""
"Podètz, d'un biais facultatiu, demandar que l'imatge o los imatges depausats "
"sus Lutim sián suprimits aprèp lor primièr afichatge (o telecargament) o al "
"cap d'un delai causit entre las prepausadas."

#: lib/Lutim/Controller/Authent.pm:27
msgid "You have been successfully logged in."
msgstr "Sètz ben estat·ada connectat·ada."

#: lib/Lutim/Controller/Authent.pm:66 themes/default/templates/logout.html.ep:3
msgid "You have been successfully logged out."
msgstr "Sètz ben estat desconnectat."

#: themes/default/templates/gallery.html.ep:31
msgid "Zoom in/out"
msgstr "Agrandir/Reduire"

#: themes/default/templates/about.html.ep:27
msgid "and on"
msgstr "e sus"

#: themes/default/templates/about.html.ep:40
msgid "arabic translation"
msgstr "traduccion en arabi"

#: themes/default/templates/about.html.ep:27
msgid "core developer"
msgstr "desvolopaire màger"

#: lib/Lutim.pm:348 lib/Lutim/Command/cron/stats.pm:157 lib/Lutim/Command/cron/stats.pm:171 lib/Lutim/Command/cron/stats.pm:188 themes/default/templates/index.html.ep:3 themes/default/templates/myfiles.html.ep:3 themes/default/templates/partial/raw.js.ep:21 themes/default/templates/partial/raw.js.ep:4 themes/default/templates/raw.html.ep:6
msgid "no time limit"
msgstr "Pas cap de limitacion de durada"

#: themes/default/templates/about.html.ep:38
msgid "occitan translation"
msgstr "traduccion en occitan"

#: themes/default/templates/about.html.ep:27
msgid "on"
msgstr "sus"

#: themes/default/templates/about.html.ep:39
msgid "paste image to upload ability"
msgstr "possibilitat de pegar un imatge per lo mandar"

#: themes/default/templates/about.html.ep:41
msgid "russian translation"
msgstr "traduccion en rus"

#: themes/default/templates/about.html.ep:36
msgid "spanish translation"
msgstr "traduccion en espanhòl"

#: themes/default/templates/about.html.ep:28
msgid "webapp developer"
msgstr "desvolopaire de la webapp"

#: themes/default/templates/about.html.ep:24
msgid "For more details, see the <a href=\"https://framagit.org/fiat-tux/hat-softwares/lutim\">homepage of the project</a>."
msgstr ""
"Per mai de detalhs, consultatz la <a href=\"https://framagit.org/fiat-tux/"
"hat-softwares/lutim\">pagina del projècte</a>."

#: themes/default/templates/index.html.ep:171 themes/default/templates/index.html.ep:215
msgid "Tiling watermark"
msgstr "Filigrana en teule"

#: themes/default/templates/index.html.ep:174 themes/default/templates/index.html.ep:218
msgid "Single watermark"
msgstr "Filigrana simpla"

#: themes/default/templates/index.html.ep:177 themes/default/templates/index.html.ep:221
msgid "No watermark"
msgstr "Cap de filigrana"
